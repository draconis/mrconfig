#!/bin/bash
HOMEDIR=$HOME
USERNAME=$(whoami)

# Installation du dépot AUR
sudo pacman -S --needed git base-devel python-pip
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si

# Applications
yay -S vim dconf zsh chrome-gnome-shell

#suppression des configuration existantes
sudo rm -fr $HOME/.zshrc 

# Thèmes pour terminator
pip install requests
mkdir -p $HOME/.config/terminator/plugins
wget https://git.io/v5Zww -O $HOME"/.config/terminator/plugins/terminator-themes.py"

# Thème GTK + GDM
git clone https://github.com/vinceliuice/WhiteSur-gtk-theme.git $HOME/White-sur/gtk
cd $HOME/White-sur/gtk
./install.sh
sudo ./tweaks.sh -g

# Icônes
git clone https://github.com/vinceliuice/WhiteSur-icon-theme.git $HOME/White-sur/icon
cd $HOME/White-sur/icon
./install.sh

# Installation des dotfiles
mr checkout
